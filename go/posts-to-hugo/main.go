package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"

	"gopkg.in/yaml.v2"
)

func main() {
	srcDir := "/Users/jjude/Desktop/blog/jjude"
	destDir := "/Users/jjude/Desktop/blog/new"
	files, err := ioutil.ReadDir(srcDir)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		entry, err := ioutil.ReadFile(srcDir + "/" + f.Name())
		if err != nil {
			fmt.Println("Error reading file: ", f.Name(), " is: ", err.Error())
			return
		}
		if f.Name() != ".DS_Store" {
			fmt.Println("dealing with: ", f.Name())
			// first 4 are ---\n
			// this is assumed
			// TODO: check if it is present and then delete
			ed := strings.Split(string(entry[4:]), "---")
			var entryMap map[string]string
			err = yaml.Unmarshal([]byte(ed[0]), &entryMap)

			if err != nil {
				fmt.Println("Error unmarshalling: ", f.Name(), " is: ", err.Error())
				return
			}

			t, _ := time.Parse("Jan 2, 2006 3:04pm MST", entryMap["PublishAt"])
			tags := strings.Split(entryMap["Tags"], ",")

			postTags := "["
			for i := range tags {
				postTags = postTags + "\"" + tags[i] + "\","
			}
			postTags = postTags + "]"

			// write to file
			data := fmt.Sprintf(`+++
title="%s"
slug="%s"
description="%s"
tags=%s
date="%s"
featuredImage="%s"
+++
%s
`,
				entryMap["Title"],
				entryMap["Slug"],
				entryMap["Excerpt"],
				postTags,
				t.Format(time.RFC3339),
				entryMap["FeaturedImage"],
				ed[1],
			)
			// write entry into file
			err = ioutil.WriteFile(destDir+"/"+f.Name(), []byte(data), 0644)
			if err != nil {
				fmt.Println("writing to ", f.Name(), " failed with: ", err.Error())
				return
			}
		}
	}
}
