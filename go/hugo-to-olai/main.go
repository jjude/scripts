package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"

	toml "github.com/pelletier/go-toml"
)

func main() {
	srcDir := "/Users/jjude/Develop/scripts/go/hugo-to-olai/src"
	destDir := "/Users/jjude/Develop/scripts/go/hugo-to-olai/olai"

	files, err := ioutil.ReadDir(srcDir)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		if f.Name() != ".DS_Store" {
			fmt.Println("dealing with: ", f.Name())
			entry, err := ioutil.ReadFile(srcDir + "/" + f.Name())
			if err != nil {
				fmt.Println("Error reading file: ", f.Name(), " is: ", err.Error())
				return
			}

			// source file is toml file
			// first 4 are ---\n
			ed := strings.Split(string(entry[4:]), "+++")
			meta, err := toml.Load(ed[0])
			if err != nil {
				fmt.Println("Error loading toml: ", err.Error())
				return
			}

			// parse date 2018-12-31T10:45:00+05:30
			t, _ := time.Parse("2006-01-02T15:04:05-07:00", meta.Get("date").(string))

			// toml can have different data types in an array; so it retunrs array of interface
			tags := meta.Get("tags").([]interface{})
			et := make([]string, len(tags))
			for i, tag := range tags {
				et[i] = fmt.Sprintf("\"%s\"", tag)
			}

			featuredImage := meta.Get("featuredImage")
			if featuredImage == nil {
				featuredImage = ""
			}

			data := fmt.Sprintf(`---
title="%s"
slug="%s"
excerpt="%s"
tags=[%s]
publish_at="%s"
featured_image="%s"
---
%s
`, meta.Get("title"),
				meta.Get("slug"),
				meta.Get("description"),
				strings.Join(et, ","),
				t.Format(time.RFC822),
				featuredImage,
				strings.TrimSpace(ed[1]),
			)

			// write to file
			err = ioutil.WriteFile(destDir+"/"+f.Name(), []byte(data), 0644)
			if err != nil {
				fmt.Println("writing to ", f.Name(), " failed with: ", err.Error())
				return
			}
		}
	}
}
