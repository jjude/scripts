#!/bin/bash

# At second :00, at minute :00, at 10am and 18pm, of every day
# https://crontab.guru/
# 0 10,18 * * * /Users/jjude/Develop/scripts/joplin.sh >> /var/log/joplin_bak.log 2>&1

BACKUP_DIR="/Users/jjude/Dropbox/backups/joplin"
JOPLIN_BIN="/usr/local/Cellar/joplin/1.0.141_1/bin/joplin"
NOW=$( date '+%F_%H:%M:%S' )

rm -rf $BACKUP_DIR/notes
$JOPLIN_BIN --profile ~/.config/joplin-desktop/ export --format md $BACKUP_DIR/notes
git add .
git commit -m "update-$NOW"