# AWS cli commands to create s3 buckets for static site hosting

## prerequisite

AWS access keys. Create them on the site


This is all for www.my-site.com. Don't forget to change the site url to your site url

## create bucket

If you leave out the region, it will be created in default region. It is better to create both www & root domain version. Then forward one to the other.

```
aws s3 mb s3://www.my-site.com/ --region us-west-1
```


```
aws s3 website s3://www.my-site.com/ --index-document index.html --error-document error.html
```

## change read access to public

```
aws s3api put-bucket-policy --bucket www.my-site.com --policy file://policy.json
```

## redirect (if needed)

If you create buckets for both www and root domains, then you need to redirect one to the other.

This needs to be done in AWS UI.

## change cname

I use cloudflare in front of aws. So I go to cloudflare to change it

## upload content

Execute this from the folder which has to be synced to s3

```
aws s3 sync . s3://www.my-site.com --exclude ".DS_Store" --exclude "*.py*" --delete
```
